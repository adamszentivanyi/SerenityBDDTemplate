# This is a test repo demsotrating the use of Serenity BDD
The boilerplate code is a copy from the serenity-bdd/serenity-cucumber-starter github repo under Apache License 2.0.
I modified the starter to fulfill the requirements for the test.

# Getting started with Serenity and Cucumber

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests,
with powerful reporting and living documentation features. It has strong support for both web
testing with Selenium, and API testing using RestAssured.

Serenity strongly encourages good test automation design, and supports several design patterns,
including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more
sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports Cucumber 6.x.

## The starter project

The best place to start with Serenity and Cucumber is to clone or download the starter project on
Github ([https://github.com/serenity-bdd/serenity-cucumber-starter](https://github.com/serenity-bdd/serenity-cucumber-starter))
. This project gives you a basic project setup, along with some sample tests and supporting classes.
There are two versions to choose from. The master branch uses a more classic approach, using action
classes and lightweight page objects, whereas
the **[screenplay](https://github.com/serenity-bdd/serenity-cucumber-starter/tree/screenplay)**
branch shows the same sample test implemented using Screenplay.

### The project directory structure

The project has build scripts for both Maven and Gradle, and follows the standard directory
structure used in most Serenity projects:

```Gherkin
src
+ main
+ test
+ java                        Test runners and supporting code
+ resources
+ features                  Feature files
+ search                  Feature file subdirectories
search_by_keyword.feature
```

Serenity 2.2.13 introduced integration with WebdriverManager to download webdriver binaries.

## The sample scenario

Both variations of the sample project uses the sample Cucumber scenario. In this scenario, Sergey (
who likes to search for stuff) is performing a search on the internet:

```Gherkin
Scenario: Searching for a term
Given User is searching product on the automation practice page
When she search for "Blouse"
Then she should see product about "Blouse"
```

### The Screenplay implementation

The sample code in the master branch uses the Screenplay pattern. The Screenplay pattern describes
tests in terms of actors and the tasks they perform. Tasks are represented as objects performed by
an actor, rather than methods. This makes them more flexible and composable, at the cost of being a
bit more wordy. Here is an example:

```java
    @Given("{actor} is searching product on the automation practice page")
public void researchingThings(Actor actor) {
        actor.wasAbleTo(NavigateTo.theAutomationPracticeHomePage());
        }

@When("{actor} search for {string}")
public void searchesFor(Actor actor, String term) {
        actor.attemptsTo(
        SearchForProduct.about(term)
        );
        }

@Then("{actor} should see product about {string}")
public void should_see_information_about(Actor actor, String term) {
        actor.attemptsTo(
        Ensure.that(SearchResultPage.PRODUCT.of(term)).isDisplayed()
        );
        }
```

Screenplay classes emphasise reusable components and a very readable declarative style, whereas Lean
Page Objects and Action Classes (that you can see further down) opt for a more imperative style.

The `NavigateTo` class is responsible for opening the Automation Page home page:

```java
public class NavigateTo {
  public static Performable theAutomationPracticeHomePage() {
    return Task.where("{0} opens the Automation practice home page",
            Open.browserOn().the(AutomationPracticePage.class));
  }
}

```

The `SearchForProduct` class does the actual search:

```java
public class SearchForProduct {
  public static Performable about(String searchTerm) {
    return Task.where("{0} searches for '" + searchTerm + "'",
            Enter.theValue(searchTerm)
                    .into(SearchForm.SEARCH_FIELD)
                    .thenHit(Keys.ENTER)
    );
  }
}
```

In Screenplay, we keep track of locators in light weight page or component objects, like this one:

```java
class SearchForm {
  static Target SEARCH_FIELD = Target.the("search field").locatedBy("#search_query_top");

}
```

The Screenplay DSL is rich and flexible, and well suited to teams working on large test automation
projects with many team members, and who are reasonably comfortable with Java and design patterns.


It does this using a standard Serenity Page Object. Page Objects are often very minimal, storing
just the URL of the page itself:

```java

@DefaultUrl("http://automationpractice.com/")
public class AutomationPracticePage extends PageObject {

}
```


The `SearchResultPage` class is a lean Page Object that locates the product titles on the results
page:

```java
public class SearchResultPage {
  public static final Target PRODUCT = Target.the("product container").locatedBy("//div[@class='product-image-container']//a[@title='{0}']");
}

```

The main advantage of the approach used in this example is not in the lines of code written,
although Serenity does reduce a lot of the boilerplate code that you would normally need to write in
a web test. The real advantage is in the use of many small, stable classes, each of which focuses on
a single job. This application of the _Single Responsibility Principle_ goes a long way to making
the test code more stable, easier to understand, and easier to maintain.

## Executing the tests

To run the sample project, run `gradle test` from the command line.

By default, the tests will run using Chrome. You can run them in Firefox by overriding the `driver`
system property, e.g.

```json
$ gradle clean test -Pdriver=firefox
```

The test results will be recorded in the `target/site/serenity` directory.


## Simplified WebDriver configuration and other Serenity extras

The sample projects both use some Serenity features which make configuring the tests easier. In
particular, Serenity uses the `serenity.conf` file in the `src/test/resources` directory to
configure test execution options.

### Webdriver configuration

The WebDriver configuration is managed entirely from this file, as illustrated below:

```java
webdriver{
    driver=chrome
    }
    headless.mode=true

    chrome.switches="""--start-maximized;--test-type;--no-sandbox;--ignore-certificate-errors;
                   --disable-popup-blocking;--disable-default-apps;--disable-extensions-file-access-check;
                   --incognito;--disable-infobars,--disable-gpu"""

```

Serenity uses WebDriverManager to download the WebDriver binaries automatically before the tests are
executed.

## Want to learn more?

For more information about Serenity BDD, you can read the [**Serenity BDD
Book**](https://serenity-bdd.github.io/theserenitybook/latest/index.html), the official online
Serenity documentation source. Other sources include:

* **[Learn Serenity BDD Online](https://expansion.serenity-dojo.com/)** with online courses from the
  Serenity Dojo Training Library
* **[Byte-sized Serenity BDD](https://www.youtube.com/channel/UCav6-dPEUiLbnu-rgpy7_bw/featured)** -
  tips and tricks about Serenity BDD
* For regular posts on agile test automation best practices, join
  the **[Agile Test Automation Secrets](https://www.linkedin.com/groups/8961597/)** groups
  on [LinkedIn](https://www.linkedin.com/groups/8961597/)
  and [Facebook](https://www.facebook.com/groups/agiletestautomation/)
* [**Serenity BDD Blog**](https://johnfergusonsmart.com/category/serenity-bdd/) - regular articles
  about Serenity BDD
