package starter.feature.common.task;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import starter.feature.common.ui.AutomationPracticePage;

public class NavigateTo {
    public static Performable theAutomationPracticeHomePage() {
        return Task.where("{0} opens the Automation practice home page",
                Open.browserOn().the(AutomationPracticePage.class));
    }
}
