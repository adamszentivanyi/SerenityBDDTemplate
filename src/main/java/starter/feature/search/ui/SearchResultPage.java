package starter.feature.search.ui;

import net.serenitybdd.screenplay.targets.Target;

public class SearchResultPage {
    public static final Target PRODUCT = Target.the("product container").locatedBy("//div[@class='product-image-container']//a[@title='{0}']");

    public static final Target ADD_TO_CART = Target.the("Add to cart").locatedBy("//a[@title='Add to cart']");

}
