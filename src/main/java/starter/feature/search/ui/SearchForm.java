package starter.feature.search.ui;

import net.serenitybdd.screenplay.targets.Target;

public class SearchForm {
   public static Target SEARCH_FIELD = Target.the("search field").locatedBy("#search_query_top");

}
