package starter.feature.cart.ui;


import net.serenitybdd.screenplay.targets.Target;

public class AddedToShoppingCartModal {

  public static Target CLOSE_MODAL = Target.the("Close modal").locatedBy("//span[@title='Close window']");

}
