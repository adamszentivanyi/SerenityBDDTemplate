package starter.feature.cart.ui;


import net.serenitybdd.screenplay.targets.Target;

public class ShoppingCartDropDown {

  public static Target SHOPPING_CART_DROP = Target.the("Shopping cart dropdown")
      .locatedBy("//div[@class='shopping_cart']");
  public static Target REMOVE_THIS_TITLE = Target.the("Remove this title")
      .locatedBy("//a[@title='remove this product from my cart']");

  public static Target SHOPPING_CART_EMPTY_TEXT = Target.the("Empty text")
      .locatedBy("//div[@class='shopping_cart']//span[contains(text(),'(empty)')]");
  public static Target CHECKOUT = Target.the("Checkout button")
      .locatedBy("//span[contains(text(),'Check out')]");

  public static Target BLOCK = Target.the("Dropdown block")
      .locatedBy("//div[contains(@class,'cart_block')]");

  public static Target SHOW_MY_SHOPPING_CART = Target.the("Show my shopping cart")
      .locatedBy("//a[@title=\"View my shopping cart\"]");
}
