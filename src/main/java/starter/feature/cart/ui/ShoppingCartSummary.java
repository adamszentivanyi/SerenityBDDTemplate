package starter.feature.cart.ui;

import net.serenitybdd.screenplay.targets.Target;

public class ShoppingCartSummary {

  public static Target REMOVE_FROM_CART = Target.the("Remove from Cart")
      .locatedBy("//a[@title='Delete']");

  public static Target SHOPPING_CART_EMPTY = Target.the("Shopping cart empty")
      .locatedBy("//p[contains(text(),\"Your shopping cart is empty.\")]");

}
