package starter.feature.cart.task;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import starter.feature.search.ui.SearchResultPage;

public class AddProductToTheCart {

    public static Performable called(String productName) {
        return Task.where("{0} adds an item called: ",
                MoveMouse.to(SearchResultPage.PRODUCT.of(productName)),
                Click.on(SearchResultPage.ADD_TO_CART));
    }
}