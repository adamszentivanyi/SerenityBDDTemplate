package starter.feature.cart.question;

import net.serenitybdd.screenplay.Question;
import starter.feature.cart.ui.ShoppingCartSummary;

public class ShoppingCart {

    public static Question<Boolean> isEmpty() {
        return actor -> ShoppingCartSummary.SHOPPING_CART_EMPTY.resolveFor(actor).isPresent();
    }
}