@manual
Feature: Shopping cart

  As a user I would like to add items to my shopping cart.
  I would like to review this items and remove them if items are not needed.

  Scenario: Adding to the shopping cart
    Given User is on the automation practice page
    When she search for "Blouse"
    And she adding "Blouse" to the shopping cart
    Then she should have one item in the shopping cart

  Scenario: Removing form the shopping cart
    Given User is on the automation practice page
    When she search for "Blouse"
    And she adding "Blouse" to the shopping cart
    And she removes it from the shopping cart
    Then she should not have anything in the shopping cart

