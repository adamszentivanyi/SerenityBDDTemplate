Feature: Search for a product

  As a user I would like to search for a product and see the products.

  Scenario Outline: Searching for a term
    Given User is searching product on the automation practice page
    When she search for "<productName>"
    Then she should see product about "<productName>"
    Examples:
      | productName                 |
      | Blouse                      |
      | Printed Dress               |
      | Faded Short Sleeve T-shirts |

