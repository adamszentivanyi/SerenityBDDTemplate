package starter.stepdefinitions.cart;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.waits.WaitUntil;
import starter.feature.cart.ui.AddedToShoppingCartModal;
import starter.feature.cart.ui.ShoppingCartDropDown;
import starter.feature.cart.ui.ShoppingCartSummary;
import starter.feature.common.task.NavigateTo;
import starter.feature.search.ui.SearchResultPage;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class CartStepDefinitions {

  @Given("{actor} is on the automation practice page")
  public void researchingThings(Actor actor) {
    actor.wasAbleTo(NavigateTo.theAutomationPracticeHomePage());
  }

  @And("{actor} adding {string} to the shopping cart")
  public void sheAddingItToTheShoppingCart(Actor actor, String productName) {
    actor.attemptsTo(
        MoveMouse.to(SearchResultPage.PRODUCT.of(productName)),
        Click.on(SearchResultPage.ADD_TO_CART)
    );
//
//    actor.attemptsTo(AddProductToTheCart.called(productName));
  }

  @Then("{actor} should have one item in the shopping cart")
  public void sheShouldHaveItInTheShoppingCart(Actor actor) {
    actor.attemptsTo(Ensure.that(SearchResultPage.ADD_TO_CART).isDisplayed());
  }

  @And("{actor} removes it from the shopping cart")
  public void sheRemovesItFromTheShoppingCart(Actor actor) {
    actor.attemptsTo(
        Click.on(AddedToShoppingCartModal.CLOSE_MODAL),
        Click.on(ShoppingCartDropDown.SHOW_MY_SHOPPING_CART),
        Click.on(ShoppingCartSummary.REMOVE_FROM_CART)
    );

  }

  @Then("{actor} should not have anything in the shopping cart")
  public void sheShouldNotHaveAnythingInTheShoppingCart(Actor actor) {
    actor.attemptsTo(WaitUntil.the(ShoppingCartSummary.SHOPPING_CART_EMPTY, isPresent()));

    //    actor.should(seeThat(ShoppingCart.isEmpty()));
  }
}