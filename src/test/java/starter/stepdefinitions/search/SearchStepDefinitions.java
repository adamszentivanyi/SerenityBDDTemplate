package starter.stepdefinitions.search;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.ensure.Ensure;
import starter.feature.common.task.NavigateTo;
import starter.feature.search.task.SearchForProduct;
import starter.feature.search.ui.SearchResultPage;

public class SearchStepDefinitions {

    @Given("{actor} is searching product on the automation practice page")
    public void researchingThings(Actor actor) {
        actor.wasAbleTo(NavigateTo.theAutomationPracticeHomePage());
    }

    @When("{actor} search for {string}")
    public void searchesFor(Actor actor, String term) {
        actor.attemptsTo(
                SearchForProduct.about(term)
        );
    }

    @Then("{actor} should see product about {string}")
    public void should_see_information_about(Actor actor, String term) {
        actor.attemptsTo(
                Ensure.that(SearchResultPage.PRODUCT.of(term)).isDisplayed()
        );
    }
}
